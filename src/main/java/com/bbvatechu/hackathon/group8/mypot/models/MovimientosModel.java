package com.bbvatechu.hackathon.group8.mypot.models;

public class MovimientosModel {

    private String id;
    private String name;
    private Double cantidad;

    public MovimientosModel(String id, String name, Double cantidad) {
        this.id = id;
        this.name = name;
        this.cantidad = cantidad;
    }

    public MovimientosModel(){};

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getCantidad() {
        return cantidad;
    }

    public void setCantidad(Double cantidad) {
        this.cantidad = cantidad;
    }
}
