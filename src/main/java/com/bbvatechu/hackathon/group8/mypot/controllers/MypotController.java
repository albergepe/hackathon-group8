package com.bbvatechu.hackathon.group8.mypot.controllers;


import com.bbvatechu.hackathon.group8.mypot.components.ForbiddenException;
import com.bbvatechu.hackathon.group8.mypot.security.AuthHandler;
import com.bbvatechu.hackathon.group8.mypot.security.JWTBuilder;
import com.bbvatechu.hackathon.group8.mypot.models.PotModel;
import com.bbvatechu.hackathon.group8.mypot.models.UserModel;
import com.bbvatechu.hackathon.group8.mypot.services.MypotService;
import com.kastkode.springsandwich.filter.annotation.Before;
import com.kastkode.springsandwich.filter.annotation.BeforeElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.PATCH, RequestMethod.POST, RequestMethod.PUT , RequestMethod.DELETE})
@RequestMapping("${base.uri}")
public class MypotController
{
    @Autowired
    JWTBuilder jwtBuilder;

    @Autowired
    MypotService mypotService;

    Logger logger = LoggerFactory.getLogger(AuthHandler.class);

    HttpServletRequest request;

    @GetMapping()
    public String index() {
        return "Página principal";
    }

   /* @GetMapping(path="/users",headers = {"Authorization"})
    @Before(@BeforeElement(AuthHandler.class))
    public List<UserModel> getUsers(){
        return mypotService.findAllUsers();
    }*/

   @GetMapping(path="/users")
    //@GetMapping(path="/users",headers={"Authorization"})
    //@Before(@BeforeElement(AuthHandler.class))
    public List<UserModel> getUsers() {
        return mypotService.findAllUsers();
    }


   // @PostMapping(path="/users",headers = {"Authorization"})
    @PostMapping("/users")
    public ResponseEntity<UserModel> postUser(@RequestBody UserModel newUser){
        return mypotService.saveUser(newUser,true);
    }

    //@GetMapping(path="/users/{id}/pots")
    @GetMapping(path="/users/{id}/pots",headers={"Authorization"})
    @Before(@BeforeElement(AuthHandler.class))
    public Optional<List<PotModel>> getUsersByIdPots(HttpServletRequest req, @PathVariable String id) {
        String user = req.getAttribute("userId").toString();
        if (user.equals(id)){
            return mypotService.findAllPotsByOwnerId(id);
        }
        throw new ForbiddenException("Recurso no disponible");
    }

    @PatchMapping(path="/users/{id}/pots")
    //@GetMapping(path="/users",headers={"Authorization"})
    //@Before(@BeforeElement(AuthHandler.class))
    public ResponseEntity<PotModel> patchUserWithPot(HttpServletRequest req,@PathVariable String id,@RequestBody PotModel newPot){
        String user = req.getAttribute("userId").toString();
        if (user.equals(id)){
            return mypotService.savePotUser(newPot,id);
        }
        throw new ForbiddenException("Recurso no disponible");
    }

    //@PostMapping(path="/pots",headers={"Authorization"})
    //@Before(@BeforeElement(AuthHandler.class))
    @PostMapping(path="/pots")
    public ResponseEntity<PotModel> postPot(@RequestBody PotModel newPot){
        return mypotService.savePot(newPot,true);
    }


    @GetMapping(path="/pots")
    //@GetMapping(path="/pots",headers = {"Authorization"})
    //@Before(@BeforeElement(AuthHandler.class))
    public List<PotModel> getPots(){
        return mypotService.findAllPots();
    }


    @GetMapping(path="/pots/{id}")
    //@GetMapping(path="/pots/{id}",headers = {"Authorization"})
    //@Before(@BeforeElement(AuthHandler.class))
    public ResponseEntity getPotById(@PathVariable String id){
        Optional<PotModel> optionalPot = mypotService.findPotById(id);
        if(optionalPot.isPresent())
        {
            return ResponseEntity.ok(mypotService.fillUsers(optionalPot.get()));
        }
        else
        {
            return new ResponseEntity<>("Elemento " + id + " no encontrado", HttpStatus.NOT_FOUND);
        }
    }



    @DeleteMapping("/pots")
    public ResponseEntity deletePot(@RequestBody PotModel delPot){
        return new ResponseEntity(HttpStatus.FORBIDDEN);

    }
    //@DeleteMapping(path="/productos/{id}",headers = {"Authorization"})
    //@Before(@BeforeElement(AuthHandler.class))
    @DeleteMapping("users/{id}/pots/")
    public ResponseEntity deleteUserPotById(@PathVariable String id){
      return mypotService.deletePotFromUser(id);
    }

}
