package com.bbvatechu.hackathon.group8.mypot.security;

import com.bbvatechu.hackathon.group8.mypot.components.ForbiddenException;
import com.kastkode.springsandwich.filter.api.BeforeHandler;
import com.kastkode.springsandwich.filter.api.Flow;
import org.jetbrains.annotations.NotNull;
import org.jose4j.jwt.JwtClaims;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class AuthHandler implements BeforeHandler {
    Logger logger = LoggerFactory.getLogger(AuthHandler.class);


    private String userInSession = "";

    public String getUserInSession() {
        return userInSession;
    }

    @Autowired
    JWTBuilder jwtBuilder;

    @Override
    public Flow handle(@NotNull HttpServletRequest request, HttpServletResponse response, HandlerMethod handler, String[] flags) throws Exception{
        logger.debug(request.getMethod() + " request is executing on " + request.getRequestURI());
        String token = request.getHeader("Authorization");

        if (token.equals("")) {
            throw new ForbiddenException("Auth token is required");
        }

        JwtClaims claims = jwtBuilder.generateParseToken(token);
        request.setAttribute("userId",claims.getClaimValue("userID").toString());
        //userInSession = claims.getClaimValue("userID").toString();
        // check if the user is still valid

        return Flow.CONTINUE;
    }

}
