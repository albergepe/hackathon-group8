package com.bbvatechu.hackathon.group8.mypot.security;

import com.bbvatechu.hackathon.group8.mypot.components.ForbiddenException;
import com.bbvatechu.hackathon.group8.mypot.controllers.MypotController;
import com.bbvatechu.hackathon.group8.mypot.models.UserModel;
import com.bbvatechu.hackathon.group8.mypot.repositories.UserRepository;
import com.bbvatechu.hackathon.group8.mypot.services.MypotService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Optional;

@RestController
//@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
@RequestMapping("${base.uri}/security")
public class SecurityController {

    @Autowired
    JWTBuilder jwtBuilder;
    @Autowired
    MypotService potService;
    @Autowired
    UserRepository userRepo;

    @PostMapping("/login")
    public String login(@RequestBody() UserModel usuario) {

        if (passwordOk(usuario)){
            String id = userRepo.findByEmail(usuario.getEmail()).get().getId();
            String token = jwtBuilder.generateToken(id,"ADMIN");
            return token + "#" + id;
        }
        throw new ForbiddenException("Not allowed here");
    }

    private boolean passwordOk(UserModel usuario) {
        Optional<UserModel> encontrado = potService.login(usuario);
        if (encontrado.isPresent()) {
            return encontrado.get().getPassword().equals(usuario.getPassword());
        }
        return false;
    }
}

