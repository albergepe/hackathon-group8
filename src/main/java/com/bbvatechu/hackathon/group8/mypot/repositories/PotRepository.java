package com.bbvatechu.hackathon.group8.mypot.repositories;


import com.bbvatechu.hackathon.group8.mypot.models.PotModel;
import com.bbvatechu.hackathon.group8.mypot.models.UserModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PotRepository extends MongoRepository<PotModel,String>
{
    Optional<List<PotModel>> findAllByOwnerId(String ownerId);

}
