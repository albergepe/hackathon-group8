package com.bbvatechu.hackathon.group8.mypot.services;

import com.bbvatechu.hackathon.group8.mypot.security.AuthHandler;
import com.bbvatechu.hackathon.group8.mypot.models.PotModel;
import com.bbvatechu.hackathon.group8.mypot.models.UserModel;
import com.bbvatechu.hackathon.group8.mypot.repositories.PotRepository;
import com.bbvatechu.hackathon.group8.mypot.repositories.UserRepository;
import org.jose4j.jwk.Use;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class MypotService
{
    Logger logger = LoggerFactory.getLogger(AuthHandler.class);

    @Autowired
    PotRepository potRepository;
    @Autowired
    UserRepository userRepository;

    public List<UserModel> findAllUsers(){
        return userRepository.findAll();
    }

    public Optional<UserModel> login(UserModel usuarioLogin){
        //UserModel encontrado = userRepository.findOne(Query.query(Criteria.where("email").is(usuarioLogin.getEmail())),UserModel.class,"usuario");
        return userRepository.findByEmail(usuarioLogin.getEmail());
    }
    public List<PotModel> findAllPots(){
        return potRepository.findAll();
    }
    public Optional <List<PotModel>> findAllPotsByOwnerId(String ownerId){
        return potRepository.findAllByOwnerId(ownerId);
    }

    public Optional<PotModel> findPotById(String id){
        return potRepository.findById(id);
    }

    public Optional<UserModel> findUserById(String id){
        return userRepository.findById(id);
    }

    public ResponseEntity<UserModel> saveUser(UserModel umodel, Boolean create){
        try {
            UserModel umcreateupdate = userRepository.save(umodel);
            if(create) {
                return new ResponseEntity<>(umcreateupdate,HttpStatus.OK);
            }
            else
            {
                return new ResponseEntity<>(umcreateupdate,HttpStatus.CREATED);
            }
        }
        catch (Exception e)
        {
            logger.error("ERROR EN EL SAVE " + e.toString());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    public ResponseEntity savePotUser(PotModel potmodel, String id){
        try {

            Optional<UserModel> userPatch = userRepository.findById(id);

            if(userPatch.isPresent())
            {
                userPatch.get().setPot(potmodel);
                return new ResponseEntity<>(null,HttpStatus.CREATED);
            }
            else
            {
                return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
            }
        }
        catch (Exception e)
        {
            logger.error("ERROR EN EL SAVE " + e.toString());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<PotModel> savePot(PotModel potmodel, Boolean create){
        try {
            PotModel potcreateupdate = potRepository.save(potmodel);
            if(create) {
                return new ResponseEntity<>(potcreateupdate,HttpStatus.OK);
            }
            else
            {
                return new ResponseEntity<>(potcreateupdate,HttpStatus.CREATED);
            }
        }
        catch (Exception e)
        {
            logger.error("ERROR EN EL SAVE " + e.toString());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public boolean existById(String id){
        return potRepository.existsById(id);
    }


    public boolean deleteById(String id) {
        try {
            potRepository.deleteById(id);
            return true;
        } catch (Exception e) {
            System.out.println("******* " + e.toString());
            return false;
        }
    }

    public ResponseEntity deletePotFromUser(String iduser){

        try {

            Optional<UserModel> userPatch = userRepository.findById(iduser);

            if(userPatch.isPresent())
            {
                userPatch.get().setPot(null);
                return new ResponseEntity<>(null,HttpStatus.CREATED);
            }
            else
            {
                return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
            }
        }
        catch (Exception e)
        {
            logger.error("ERROR EN EL SAVE " + e.toString());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public PotModel fillUsers(PotModel potModel) {
        List<UserModel> newUserList = new ArrayList<UserModel>();
        UserModel tmpUser = new UserModel();
        for (UserModel user:  potModel.getUsers()
             ) {
            if (userRepository.findById(user.getId()).isPresent()){
                tmpUser = userRepository.findById(user.getId()).get();
                tmpUser.setPassword("");
                newUserList.add(tmpUser);
            };

        }
        potModel.setUsers(newUserList);
        return potModel;
    }

/*



    public boolean delete(ProductoModel producto){
        try{
            productRepository.delete (producto);
            return true;
        }
        catch(Exception e)
        {
            System.out.println("******* " + e.toString());
            return false;
        }
    }


    }*/
}
