package com.bbvatechu.hackathon.group8.mypot.models;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.stereotype.Repository;

import java.util.List;


@Document(collection = "user")
public class UserModel {

    @Id
    @NotNull
    private String id;
    private String name;
    private String email;
    private String password;
    private PotModel pot;

    public UserModel(){}

    public UserModel(String id, String name, String email,String password,PotModel pot) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.password = password;
        this.pot = pot;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public PotModel getPot() {
        return pot;
    }

    public void setPot(PotModel pot) {
        this.pot = pot;
    }
}
