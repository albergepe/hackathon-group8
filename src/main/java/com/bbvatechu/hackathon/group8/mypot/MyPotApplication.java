package com.bbvatechu.hackathon.group8.mypot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.kastkode.springsandwich.filter","com.bbvatechu.hackathon.group8.mypot"})
public class MyPotApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyPotApplication.class, args);
	}

}
