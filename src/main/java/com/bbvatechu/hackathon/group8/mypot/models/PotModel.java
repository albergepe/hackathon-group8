package com.bbvatechu.hackathon.group8.mypot.models;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Document(collection = "pot")
public class PotModel {

   @Id
    @NotNull
    private String bot_id;
    private String bot_name;
    private Double bot_balance;
    private String ownerId;
    private List<MovimientosModel> movimientos;
    private List<UserModel> users;

    public PotModel(){}

    public PotModel(@NotNull String bot_id, String bot_name, Double bot_balance, List<MovimientosModel> movimientos, List<UserModel> users) {
        this.bot_id = bot_id;
        this.bot_name = bot_name;
        this.bot_balance = bot_balance;
        this.movimientos = movimientos;
        this.users = users;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public List<MovimientosModel> getMovimientos() {
        return movimientos;
    }

    public void setMovimientos(List<MovimientosModel> movimientos) {
        this.movimientos = movimientos;
    }

    public List<UserModel> getUsers() {
        return users;
    }

    public void setUsers(List<UserModel> users) {
        this.users = users;
    }

    public String getBot_id() {
        return bot_id;
    }

    public void setBot_id(String bot_id) {
        this.bot_id = bot_id;
    }

    public String getBot_name() {
        return bot_name;
    }

    public void setBot_name(String bot_name) {
        this.bot_name = bot_name;
    }

    public Double getBot_balance() {
        return bot_balance;
    }

    public void setBot_balance(Double bot_balance) {
        this.bot_balance = bot_balance;
    }
}
