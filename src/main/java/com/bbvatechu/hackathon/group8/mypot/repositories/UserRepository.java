package com.bbvatechu.hackathon.group8.mypot.repositories;


import com.bbvatechu.hackathon.group8.mypot.models.UserModel;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends MongoRepository<UserModel,String>
{
    Optional<UserModel> findByEmail(String email);

}
